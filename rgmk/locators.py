import pymel.core as pm

from . import jn
from .constants import LOC
from .utils import find


def fix_locator_names():
    for loc in find(jn('*', LOC, '_*'), type="transform"):
        if not loc.name().endswith(LOC):
            loc_incr = loc.name()[-2:]
            loc.rename(loc.name()[:-3].replace(LOC, jn(loc_incr, LOC)))


def rename_locators(selector=":*locators|*"):
    locators = [lc for lc in find(selector, transforms=True)
                if lc.getShape().nodeType() == "locator"]

    for loc in locators:
        try:
            tail = loc.nodeName().split(LOC)[1]
            incr = tail.lstrip('_')
            int(incr)
        except IndexError:
            # There was no increment trailing the node name
            pass
        except ValueError:
            # There was information after LOC but it was not a number
            pass
        else:
            name = loc.nodeName()[:-len(tail)]
            new_name = name.replace(LOC, jn(incr.zfill(2), LOC))
            loc.rename(new_name)
