import pymel.core as pm

from . import jn
from . import constants
from .constants import LOC, NUL


def find(*args, **kwargs):
    first_only = kwargs.pop("first_only", False)

    selector_list = pm.ls(*args, **kwargs)

    if first_only:
        return selector_list[0] if selector_list else []

    return selector_list


def emptyGroup(name, **kwargs):
    kwargs.pop("empty", False)
    kwargs.pop("e", False)

    return pm.group(name=name, empty=True, **kwargs)


def node_basename(node, keep_digits=False):
    name = str(node)

    prefixes = constants.prefixes
    suffixes = constants.suffixes

    for pre in prefixes:
        name = name.replace(pre, '')

    for suf in suffixes:
        name = name.replace(suf, '')

    if not keep_digits:
        name = name.rstrip('0123456789')

    return name


def traverse(element, child_func, end_func=None):
    if end_func is None:
        end_func = child_func

    if element.children is not None:
        for child in element.children:
            child_func(child)
    else:
        end_func(element)


def list_to_chain(names, mirror=None, typehint=None):

        prefix = mirror if mirror is not None else ""
        suffix = LOC if typehint is None else typehint
        tpl = "{prefix}{name}*{suffix}"

        chain = []
        for name in names:
            selector = tpl.format(prefix=prefix, name=name, suffix=suffix)
            chain.append(selector)

        return tuple(chain)


def get_end_of_chain(selector, chainlist, first=False):
    """ Returns the last (or first) item in a chain """

    subchain = sorted([node for node in chainlist if selector in str(node)])

    if not subchain:
        raise RuntimeError("No matching subchains found")

    return subchain[0] if first else subchain[-1]


def get_hierarchy(root, end):

    if not root.isParentOf(end):
        pm.displayError("{0} is not an ancestor of {1}".format(root, end))
        return False

    path = end.fullPath().split('|')

    start_index = path.index(str(root))
    end_index = path.index(str(end))

    return [pm.PyNode(n) for n in path[start_index:end_index+1]]


def snap_to(object1, object2):
    # Temporarily constrain to snap object 1 to object 2
    pm.delete(pm.pointConstraint(object2, object1))

    #FIXME: Broken
    #object1 = pm.PyNode(object1)
    #object2 = pm.PyNode(object2)

    #obj1_rp = object2.getRotatePivotTranslation()

    #obj2_pos = object2.getTranslation(worldSpace=True)
    #obj2_rp = object2.getRotatePivotTranslation()

    #object1.setTranslation((obj2_pos[0] + obj2_rp[0] - obj1_rp[0],
    #                       obj2_pos[1] + obj2_rp[1] - obj1_rp[1],
    #                       obj2_pos[2] + obj2_rp[2] - obj1_rp[2]))


def null_with_group(name):
    # TODO: Check and handle local transforms
    node = pm.PyNode(name)

    if not node:
        pm.displayError(jn("Could not find name ", name))
        return None

    tmatrix = node.getTransformation()
    pm.makeIdentity(node, r=True, t=True, s=True, n=False)
    grp = pm.group(node, name=jn(str(node), NUL))
    grp.setTransformation(tmatrix)

    return grp


def orient_joint_chain(jnt):
    children = pm.listRelatives(jnt,
                                children=True,
                                type="joint")

    if children:

        # then orient it like a parent joint
        pm.joint(jnt,
                 edit=True,
                 zso=True,
                 oj='xyz',
                 sao='yup',
                 ch=True)

        # and for each of its children
        for child in children:
            orient_joint_chain(child)

    else:
        # Follow the parent joint
        # pm.joint(jnt, edit=True, o=(0, 0, 0))

        # This is one of those commands where the arguments really
        # only work in very specific combinations. Be wary, and
        # watch the console for warnings. Then treat them as errors
        pm.joint(jnt, edit=True, oj="none", zso=True)
