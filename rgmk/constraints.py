import pymel.core as pm

from . import jn
from .utils import find
from .utils import null_with_group
from .utils import snap_to


def utility_constraint(constraint_func, *args, **kwargs):
    """ Wraps common options used by all Maya constraint functions """
    def wrapper(*args, **kwargs):
        driver = pm.PyNode(args[0])
        driven = pm.PyNode(args[1])

        snap = kwargs.pop("snap", None)
        offset = kwargs.pop("offset", None)
        make_null = kwargs.pop("null", True)
        weight = kwargs.pop("weight", 1.0)
        pivot = kwargs.pop("pivot", None) # initial, snap, offset

        pivots = None
        if pivot is not None and pivot not in ["initial", "snap", "offset"]:
            try:
                node = pm.PyNode(pivot)
            except pm.MayaNodeError:
                raise ValueError("Invalid value passed for pivot")
            else:
                pivots = node.getTranslation(worldSpace=True)

        if pivot == "initial":
            pivots = driver.getTranslation(worldSpace=True)

        # We either want to snap to the driven object, or we want to
        # specify a specific object to snap to instead
        if snap is True:
            snap_to(driver, driven)
        elif snap:
            try:
                # To get here we'd need to not be True but also not be a
                # falsey value, including None, False, and the empty string
                snap_target = pm.PyNode(snap)
            except pm.MayaNodeError:
                raise ValueError("Snap must either have boolean value or be "
                    "parseable to a PyNode")
            else:
                snap_to(driver, snap_target)

        if pivot == "snap":
            pivots == driver.getTranslation(worldSpace=True)

        # Regardless, we can then offset from our current position
        # whether we used a form or snapping or not
        if offset is not None:
            pm.move(offset[0], offset[1], offset[2], driver, relative=True)

        if pivot == "offset":
            pivots == driver.getTranslation(worldSpace=True)

        nul = None
        if make_null:
            nul = null_with_group(driver)

        if pivots is not None:
            driver.setPivots(pivots, worldSpace=True)
        else:
            driver.zeroTransformPivots()

        constraint = constraint_func(driver, driven, weight=weight)

        return (nul, constraint)
    return wrapper


@utility_constraint
def polevector_constrain(driver, driven, weight=1.0):
    return pm.poleVectorConstraint(driver, driven,
                                   name=jn(str(driver),
                                           "_poleVectorConstraint01"),
                                   w=weight)


@utility_constraint
def point_constrain(driver, driven, weight=1.0):
    return pm.pointConstraint(driver, driven,
                              name=jn(str(driver), '_pointConstraint01'),
                              w=weight)


@utility_constraint
def parent_constrain(driver, driven, weight=1.0):
    return pm.parentConstraint(driver, driven,
                               name=jn(str(driver), '_parentConstraint01'),
                               maintainOffset=True,
                               w=weight)


@utility_constraint
def orient_constrain(driver, driven, weight=1.0):
    return pm.orientConstraint(driver, driven,
                               name=jn(str(driver), '_orientConstraint01'),
                               maintainOffset=True,
                               w=weight)


def decomposeMatrix(control,
                    target,
                    constrain=None,
                    group_target=False):
    """Attaches a control object's transforms to a target object's transforms"""

    if constrain is None:
        constrain = ["r", "t", "s"]

    ctrl = find(control, first_only=True, transforms=True)

    dmatrix = ctrl.listConnections(type="decomposeMatrix")
    if not dmatrix:
        dmatrix = pm.createNode("decomposeMatrix",
                                name=jn(str(ctrl), "_dmatrix"))

        ctrl.worldMatrix.connect(dmatrix.inputMatrix, force=True)
    else:
        dmatrix = dmatrix[0]

    node = (find(target, first_only=True) if not group_target
            else null_with_group(target))

    if not node:
        pm.error("Target node not found")

    if "r" in constrain:
        # rotate
        dmatrix.outputRotate.connect(node.rotate, force=True)

    if "t" in constrain:
        # translate
        dmatrix.outputTranslate.connect(node.translate, force=True)

    if "s" in constrain:
        # scale
        dmatrix.outputScale.connect(node.scale, force=True)


def mk_driven_key(driver, driven):
    raise NotImplementedError("Driven keys via code are not yet implemented")


def attach_main_control(maya_rig, ctrl_transform):

    main_ctrl = find(ctrl_transform, first_only=True, transforms=True)

    pm.parent(main_ctrl, maya_rig.name)

    decomposeMatrix(main_ctrl, maya_rig.rot, constrain="r", group_target=False)
    decomposeMatrix(main_ctrl, maya_rig.pos, constrain="t", group_target=False)
    decomposeMatrix(main_ctrl, maya_rig.scale, constrain="s", group_target=False)
