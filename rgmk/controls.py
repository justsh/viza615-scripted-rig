import pymel.core as pm
import pymel.core.nodetypes as nt

from . import jn
from . import constants  # L, R, CTRL, PLVC, COLORS
from .utils import find


def set_overrides(*args, **kwargs):
    nodes = None
    num_args = len(args)

    if not num_args:
        args = [jn('*', constants.CTRL, '*'), jn('*', constants.PLVC, '*')]

    nodes = find(*args, shapes=True)

    for node in nodes:

        node.overrideEnabled.set(True)
        node.overrideShading.set(False)

        try:
            # Not all curve types support these flags
            node.overrideTexturing(False)
        except TypeError:
            pm.mel.mprint("Texturing overrides not available for {0}. "
                          "Skipping...\n".format(str(node)))

        name = str(node)

        if "color" in kwargs:
            c = kwargs["color"]
        elif constants.L in name:
            c = constants.COLORS["lightblue"]
        elif constants.R in name:
            c = constants.COLORS["red"]
        elif "main_" in name.lower() or "master_" in name.lower():
            c = constants.COLORS["lightyellow"]
        else:
            c = constants.COLORS["lightyellow"]

        node.overrideColor.set(c)


def copy_template(name, template, scale=None, template_group=":templates"):
    """ Copies a control template from the provided group """

    copied_control = pm.duplicate(template, name=name)[0]

    if scale is not None:
        sx, sy, sz = scale
        copied_control.scale.set(sx, sy, sz, type="double3")

    pm.parent(copied_control, world=True)

    pm.makeIdentity(copied_control, apply=False, s=True)

    return copied_control


def circle(name, radius=1, normal=[0, 1, 0], **kwargs):
    """ Make a new circular control curve """
    return pm.circle(name=name, radius=radius, normal=normal, **kwargs)[0]


def cube(name, width=1.0):
    w = width / 2.0
    return pm.curve(name=name,
                    degree=1,
                    p=[[-w, w, w], [w, w, w],
                       [w, w, -w], [-w, w, -w],
                       [-w, w, w], [-w, -w, w],
                       [-w, -w, -w], [w, -w, -w],
                       [w, -w, w], [-w, -w, w],
                       [w, -w, w], [w, w, w],
                       [w, w, -w], [w, -w, -w],
                       [-w, -w, -w], [-w, w, -w]])


def cluster_curve(curves=None, group_endpoints=True):
    # Based on /scripts/startup/clusterCurve.mel

    if curves is None:
        # First look for shapes in the selection (highly unlikely)
        curves = find(sl=True, type="nurbsCurve")
        if not curves:
            # Then look for transforms and add any of their shapes
            for node in find(sl=True, transforms=True):
                shapes = node.getShapes(type="nurbsCurve")
                if shapes:
                    curves.extend(shapes)

        if not curves:
            raise RuntimeError("Failed to find suitable curves in selection")

    for curve in curves:
        num_cvs = curve.numCVs()
        clusters = []

        if num_cvs >= 4 and group_endpoints:
            # attach control points on the ends together

            # Index of the second, end, and second-to-last clusters
            ia = 1
            e = num_cvs - 1
            ib = e - 1

            for i in range(num_cvs):
                cl, clhandle = pm.cluster(curve.cv[i], relative=True)
                clusters.append([cl, clhandle])

            # Move the second cluster under the first cluster handle
            pm.cluster(clusters[ia][0], edit=True, weightedNode=[clusters[0][1], clusters[0][1]])
            # and the second-to-last cluster under the last cluster handle
            pm.cluster(clusters[ib][0], edit=True, weightedNode=[clusters[e][1], clusters[e][1]])

            pm.delete(clusters[ia][1])
            pm.delete(clusters[ib][1])

            # Fix the list that we return to match the deletions
            clusters[ia][1] = clusters[0][1]
            clusters[ib][1] = clusters[e][1]

            return clusters
        else:
            # act like clusterCurve.mel
            for i in range(num_cvs):
                pm.cluster(curve.cv[i], relative=True)

            return clusters


def replace_cluster(clusterHandle, weightedNode, align=True, null=True):
    # FIXME: Curves jump when moved to the new transform,
    # even if it is nulled. Might be easier to just parent,
    # it'd take some unnecessary work. Clusters are already nulled...
    from .utils import snap_to
    from .utils import null_with_group

    try:
        clh = nt.Transform(clusterHandle)
        wn = nt.Transform(weightedNode)
    except pm.MayaNodeError:
        raise

    # snap the new transform to the old one
    if align:
        snap_to(wn, clh)

    if null:
        # FIXME: Ignoring...
        if False:
            null_with_group(wn)
            pm.makeIdentity(wn, apply=True)

    for cl in clh.listConnections(type="cluster"):
        pm.cluster(cl, edit=True, bindState=True, weightedNode=[wn, wn])
