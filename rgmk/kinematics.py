import pymel.core as pm
import pymel.core.nodetypes as nt

from . import jn
from . import controls
from . import constraints
from .constants import LOC, JNT, CTRL, IKH, PLVC, SPNG, SPLN, RBN, NUL
from .utils import node_basename


def singleChain_ik(start_joint, end_joint, parent=None):
    """
    Makes a single IKH Handle with the pivots of its associated joints
    on a parent group
    """

    solver = "ikSCsolver"

    ik_name = str(end_joint).replace(JNT, IKH)
    ik_sc, effector = pm.ikHandle(n=ik_name,
                                  sj=start_joint,
                                  ee=end_joint,
                                  sol=solver)

    trans = ik_sc.transformationMatrix()

    ik_grp = pm.group(ik_sc, name=jn(ik_name, NUL))
    pm.makeIdentity(ik_sc, apply=False, t=True, r=True, s=True)

    ik_grp.setTransformation(trans)
    ik_grp.zeroTransformPivots()

    if parent is not None:
        pm.parent(str(ik_grp), parent)

    return (ik_grp, ik_sc)


def rotatePlane_ik(start_joint, end_joint, parent=None, mk_plvc=False):
    """
    Makes a single IKH Handle with the pivots of its associated joints
    on a parent group
    """

    solver = "ikRPsolver"

    ik_name = str(start_joint).replace(JNT, IKH)
    ik_rp, effector = pm.ikHandle(n=ik_name,
                                  sj=end_joint,
                                  ee=start_joint,
                                  sol=solver)

    trans = ik_rp.transformationMatrix()
    ik_nul = pm.group(ik_rp, name=jn(ik_name, NUL))
    pm.makeIdentity(ik_rp, apply=False, r=True, t=True, s=True)

    ik_nul.setTransformation(trans)
    ik_nul.zeroTransformPivots()

    if mk_plvc:
        ctrl = controls.copy_template(ik_name.replace(IKH, PLVC),
                                      "polevector", scale=[1, 1, 1])
        plvc = constraints.polevector_constrain(ik_rp,
                                                ctrl,
                                                nt.Joint(start_joint).getParent(),
                                                offset=[0, 0, 1])

    if parent is not None:
        pm.parent(str(ik_nul), parent)
        if mk_plvc:
            pm.parent(plvc, parent)

    return (ik_nul, ik_rp)


def spring_ik(start_joint, bend_joint, end_joint, parent=None):

    pm.mel.ikSpringSolver()

    ik_end_name = str(end_joint).replace(JNT, SPNG)
    ik_bend_name = str(bend_joint).replace(JNT, SPNG)
    ik_group_name = str(start_joint).replace(JNT, jn(SPNG, NUL))

    solver = "ikSpringSolver"

    ik_end, effector = pm.ikHandle(n=ik_end_name,
                                   sj=bend_joint,
                                   ee=start_joint,
                                   sol=solver)

    ik_bend, effector = pm.ikHandle(n=ik_bend_name,
                                    sj=end_joint,
                                    ee=start_joint,
                                    sol=solver)

    if (ik_bend.getTranslation() != ik_end.getTranslation()):
        pm.displayWarning("Spring IK handle positions differ")

    pos = nt.Joint(start_joint).getTranslation(worldSpace=True)

    ik_grp = pm.group(ik_bend, ik_end, name=ik_group_name)
    pm.makeIdentity(ik_bend, apply=False, r=True, t=True, s=True)
    pm.makeIdentity(ik_end, apply=False, r=True, t=True, s=True)

    ik_grp.setTranslation(pos, worldSpace=True)
    ik_grp.zeroTransformPivots()

    if parent is not None:
        pm.parent(ik_group_name, parent)

    return (ik_grp, ik_end, ik_bend)


def spline_ik(start_joint, end_joint):

    solver = "ikSplineSolver"

    basename = node_basename(start_joint, keep_digits=False)
    name = jn(basename, SPLN, IKH)
    ik_spline, eff, curve = pm.ikHandle(n=name,
                                        sj=start_joint,
                                        ee=end_joint,
                                        sol=solver,
                                        simplifyCurve=False,
                                        parentCurve=False)

    curve.rename(jn(basename, CTRL, SPLN))
    curve.select()
    cluster_list = controls.cluster_curve(group_endpoints=True)

    # The endpoints will be sharing transforms now
    clusterTransforms = set()
    clusterShapes = []
    clusters = []

    for cl, clt in cluster_list:
        clusters.append(cl)
        clusterTransforms.add(clt)

    for clt in clusterTransforms:
        clusterShapes.extend(clt.getShapes(type="clusterHandle"))

    import re as __re
    re_uint = __re.compile('\d+')  # positive integer strings

    # Returns the integer value of the first number encountered in the string
    isolate_uint = lambda node: int(re_uint.search(str(node)).group())

    clusters.sort(key=isolate_uint)
    clusterTransforms = sorted(clusterTransforms, key=isolate_uint)
    clusterShapes.sort(key=isolate_uint)

    for i, cl in enumerate(clusters, 1):
        # Rename clusters before shapes to avoid Maya's auto-renaming
        cl.rename(jn(basename, "_cluster", str(i).zfill(2)))

    for i, shape in enumerate(clusterShapes, 1):
        # Rename shapes before transforms to avoid Maya's auto-renaming
        shape.rename(jn(basename, "_clusterHandle", str(i).zfill(2), "Shape"))

    clusterTransforms[0].rename(jn(basename, "_clusterHandle", "01"))
    for i, clt in enumerate(clusterTransforms[1:-1], 3):
        clt.rename(jn(basename, "_clusterHandle", str(i).zfill(2)))
    clusterTransforms[-1].rename(jn(basename, "_clusterHandle",
        str(len(clusterShapes)).zfill(2)))

    return (ik_spline, curve, clusterTransforms)


def ribbon_spline(locators, **kwargs):
    # REF: "http://joseantoniomartinmartin.com/2010/06/20/tutorial-ribbon-stretchy-spine-rig/
    ##
    key_removed = False
    keys = ["patchesU", "u", "degree", "d", "length"]
    for k in keys:
        val = kwargs.pop(k, None)
        if val:
            key_removed = True
            pm.displayWarning("Parameter \"{1}\" is calculated automatically "
                              "to ensure proper ribbon generation")

    if key_removed:
        pm.displayInfo(jn("Argument Blacklist: ", ', '.join(keys), "\n\n"))
        pm.displayInfo("Arguments ignored. See Script Editor for details")

    ##
    if not (kwargs.get("name") and kwargs.get("n")):
        name = jn(node_basename(locators[0], keep_digits=False), RBN, IKH)

    if not (kwargs.get("axis") and kwargs.get("ax")):
        kwargs["axis"] = [0, 1, 0]

    ##
    v_count = len(locators)

    if v_count > 5:
        pm.warning("Using more than 5 locators may cause unexpected behaviour")

    # other flags: pivot, axis,
    ribbon = pm.nurbsPlane(name=name,
                           lengthRatio=v_count,
                           degree=3,
                           patchesU=1,
                           patchesV=v_count,
                           constructionHistory=False,
                           **kwargs)[0]

    pm.rebuildSurface(ribbon,
                      replaceOriginal=True,
                      rebuildType=0,
                      end=1,
                      keepRange=True,
                      keepControlPoints=False,
                      keepCorners=False,
                      direction=0,  # rebuild in U Only
                      spansU=1,
                      degreeU=1,
                      spansV=v_count,
                      degreeV=3,
                      tolerance=0)

    for i, loc in enumerate(locators):
        vpos = 1.0 - (i + 0.5) / v_count
        follicle(ribbon, uPos=0.5, vPos=vpos)

        print(loc.nodeName())
        name = jn(loc.nodeName().replace(LOC, JNT))
        jnt = pm.joint(name=name)
        #pm.parent(jnt, f.getParent())
        jnt.translate.set(0, 0, 0)
        jnt.radius.set(0.2)

    # TODO: Make driver joints
    # TODO: Figure out how to snap follicles to existing transforms
    raise NotImplementedError("Ribbon Spline Implementation not finished")

    return ribbon


def follicle(surface, uPos=0.0, vPos=0.0):

    try:
        # maybe you gave me a transform?
        surface = surface.getShape()
    except AttributeError:
        # treat the input like it's a surface then
        pass

    # create a name with frame padding
    name = jn(node_basename(surface), '_follicle001')

    follicle = pm.createNode('follicle', name=name)

    try:
        surface.local.connect(follicle.inputSurface)
    except:
        # TODO: Make this robust
        return False

    transform = follicle.getParent()

    surface.worldMatrix.connect(follicle.inputWorldMatrix)
    follicle.outRotate.connect(transform.rotate)
    follicle.outTranslate.connect(transform.translate)
    follicle.parameterU.set(uPos)
    follicle.parameterV.set(vPos)

    return follicle


def rivet():
    raise NotImplementedError("Rivets not yet available")
