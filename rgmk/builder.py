# Justin Sheehy

import pymel.core as pm
import pymel.core.nodetypes as nt

from . import MayaRig
from . import jn
from .constants import LOC, JNT, GRP, NUL
from .utils import find
from .utils import emptyGroup
from .utils import snap_to


def base_hierarchy(name):

    # create top level group
    # because the group might already exist, we should
    # record the name Maya returns us and use it instead
    grp = pm.group(name=name, empty=True)

    s = emptyGroup(jn("scale", NUL), parent=grp)
    t = emptyGroup(jn("pos", NUL), parent=s)
    r = emptyGroup(jn("rot", NUL), parent=t)

    g = emptyGroup(jn("geo", GRP), parent=grp)
    b = emptyGroup(jn("bones", GRP), parent=r)
    c = emptyGroup(jn("ctrls", GRP), parent=r)
    e = emptyGroup(jn("extras", GRP), parent=grp)

    src = emptyGroup(jn("source", GRP), parent=grp)

    pm.select(deselect=True)

    return MayaRig(grp, s, t, r, g, b, c, e, src)


def joint_chain(locator_list, return_root=False):

    chain = []

    for locator in locator_list:
        try:
            # Make sure the objects in the list can all be represented
            # as transforms. If not, skip them; they are not locators
            locator = nt.Transform(locator)
        except pm.MayaNodeError:
            continue

        position = locator.translate.get()

        # After we've found the locator, replace LOC with JNT at the end
        # of the string
        if LOC in str(locator):
            joint_name = str(locator).replace(LOC, JNT)

            # Request the calculated joint chain, save the one maya gives us
            joint = pm.joint(name=joint_name, p=position)

            # adding the new joint into the list
            chain.append(joint)

    # return the list of new joints
    return chain if not return_root else chain[0]


def locators_from_joints(scale=None, hide=True, *args, **kwargs):
    joints = find(jn('*', JNT), type="joint")

    locator_group = emptyGroup("found_locators")

    for joint in joints:
        name = joint.name().replace(JNT, LOC)
        locator = pm.spaceLocator(name=name)

        if scale is not None:

            locatorShape = locator.listRelatives(type="locator")[0]

            sx, sy, sz = scale
            locatorShape.localScaleX.set(sx)
            locatorShape.localScaleY.set(sy)
            locatorShape.localScaleZ.set(sz)

        snap_to(locator, joint)

        pm.parent(locator, locator_group)

    pm.select(locator_group)

    if hide:
        locator_group.hide()
