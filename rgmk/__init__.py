"""
author: Justin Sheehy

rig automation package
"""

import pymel.core as pm
from collections import namedtuple

try:
    pm.loadPlugin("decomposeMatrix", quiet=True)
except:
    raise


MayaRig = namedtuple("MayaRig",
                     "name scale pos rot geo bones ctrls extras src")


jn = lambda *args: ''.join(list(args))
