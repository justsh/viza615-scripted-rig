import pymel.core as pm

from . import jn
from .constants import GEO, JNT, CPY
from .utils import find


def parent_bind(parent):
    """
    Looks for all geometry named as GEO and attempts to parent it to
    a joint of the same name, should one exist
    """

    pm.select(deselect=True)

    copied_geometry = []

    for geometry in find(jn('*', GEO)):
        joint_name = geometry.name().replace(GEO, JNT)
        joint_name = joint_name[joint_name.rfind('|'):]

        if find(joint_name, first_only=True):
            geometry_copy = pm.duplicate(geometry, name=jn(geometry, CPY))
            geometry_copy = pm.parent(geometry_copy, parent)

            pm.parentConstraint(joint_name, geometry_copy, maintainOffset=True)

            copied_geometry.append(geometry_copy)

    return copied_geometry


def rigid_bind():
    raise NotImplementedError("Rigid Bind not yet implemented")


def smooth_bind():
    raise NotImplementedError("Smooth Bind not yet implemented")
