import pymel.core as pm

from . import jn
from .constants import JNT
from .utils import find


def toggle_joint_localAxis(visibility):
    joints = find(jn('*', JNT))

    for jnt in joints:
        if pm.toggle(jnt, query=True, localAxis=True) is not visibility:
            pm.toggle(jnt, localAxis=True)


def interactive_resize_joints():
    pm.mel.jdsWin()


def toggle_ctrls():
    try:
        __ctrl_show = not __ctrl_show
    except NameError:
        __ctrl_show = False

    for item in pm.ls("*_CTRL*", transforms=True):
        if item.getAttr("visibility") is not __ctrl_show:
            if __ctrl_show:
                item.show()
            else:
                if(item.name() != "main_CTRL"):
                    item.hide()


def toggle_geo():
    try:
    __geo_show = not __geo_show
    except NameError:
        __geo_show = True

    for item in pm.ls("*:*_GEO"):
        if item.getAttr("visibility") is not __geo_show:
            if __geo_show:
                item.show()
            else:
                item.hide()


def toggle_loc():
    try:
    __loc_show = not __loc_show
    except NameError:
        __loc_show = False

    item = pm.ls("lof_locators|")[0]
    if item.getAttr("visibility") is not __loc_show:
        if __loc_show:
            item.show()
        else:
            item.hide()


def toggle_reverse_jnt():
    try:
    __rv_show = not __rv_show
    except NameError:
        __rv_show = False

    for item in pm.ls("*RV_*JNT*"):
        if item.getAttr("visibility") is not __rv_show:
            if __rv_show:
                item.show()
            else:
                item.hide()
