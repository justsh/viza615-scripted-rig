"""
A word about constants: these constants are intended to be imported using
from <name> import <constant>
precisely because they are constants and are thus not intended to be update
during the runtime of the program

Do NOT import settings in this way, because if one module updates a setting
during the runtime and another tries to access it, they will retain the value
from the imported setting in their namespace
"""

# mirror prefixes
L = "L_"            # Left
R = "R_"            # Right
B = "BACK_"         # Back
F = "FRONT_"        # Front
RV = "RV_"          # Reverse

# naming convention suffixes
LOC = "_LOC"        # Locators
GEO = "_GEO"        # Geo
GRP = "_GRP"        # Group
NUL = "_NUL"        # Null
JNT = "_JNT"        # Joint
CTRL = "_CTRL"      # Control
IKH = "_IK"         # IK Handle
PLVC = "_PLVC"      # Pole Vector
CPY = "_CPY"        # Copy
SPNG = "_SPRING"    # Spring IK
SPLN = "_SPLINE"    # Spline IK
RBN = "_RIBBON"     # Ribbon IK
TMP = "_TMP"        # Temporary

# Maya color indicies
COLORS = {
    "lightblue": 18,
    "red": 13,
    "lightyellow": 22
}

prefixes = [L, R, F, B, RV]
suffixes = [LOC, GEO, GRP, NUL, JNT, CTRL, IKH, PLVC, CPY, SPNG, SPLN,
            RBN, TMP]
