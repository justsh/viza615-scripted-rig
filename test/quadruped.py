import pymel.core as pm

import rgmk
import rgmk.builder
import rgmk.kinematics
import rgmk.constraints
import rgmk.controls
import rgmk.utils

from rgmk import jn
from rgmk.constants import (L, R, B, RV,
                            LOC, GRP, NUL, JNT, CTRL, IKH, PLVC, SPNG)
from rgmk.utils import find


def init_rig(rig_name="Quadruped", mctrl_name=jn("main", CTRL)):

    rig_node = find(rig_name, first_only=True, transforms=True)
    rig_result = None

    if not rig_node:

        # TODO: give option not to make one by default
        print("Could not find rig {0}".format(rig_name))
        print("Making one for you")
        rig_result = rgmk.builder.base_hierarchy(rig_name)

        # Make controls here if necessary

        mctrl = rgmk.controls.circle(mctrl_name, radius=3)
        rgmk.constraints.attach_main_control(rig_result, mctrl)
    else:
        print("Rig {0} found as {1}".format(rig_name, rig_node.longName()))
        rig_result = rgmk.MayaRig(rig_name,
                                  find(jn("scale", NUL), first_only=True),
                                  find(jn("pos", NUL), first_only=True),
                                  find(jn("rot", NUL), first_only=True),
                                  find(jn("geo", GRP), first_only=True),
                                  find(jn("bones", GRP), first_only=True),
                                  find(jn("ctrls", GRP), first_only=True),
                                  find(jn("extras", GRP), first_only=True),
                                  find(jn("source", GRP), first_only=True))

    pm.select(rig_result.name, replace=True)
    return rig_result


def mk_body(rig):

    root_locator = find(jn("ROOT", LOC), first_only=True, transforms=True)

    if not root_locator:
        raise pm.MayaNodeError("Expected root locator was not found")

    root_joint = rgmk.builder.joint_chain([root_locator], return_root=True)

    root_up = rgmk.utils.list_to_chain(["neck", "head"])
    root_down = rgmk.utils.list_to_chain(["spine"])
    root_pelvis = rgmk.utils.list_to_chain(["pelvis"])
    root_tail = rgmk.utils.list_to_chain(["tail"])

    find_loc = lambda p: [l for l in find(p, transforms=True)
                          if l.getShape().nodeType() == "locator"]

    head_locators = find_loc(root_up)
    spine_locators = find_loc(root_down)
    pelvis_locators = find_loc(root_pelvis)
    tail_locators = find_loc(root_tail)

    # attaching the hips to the root joint
    pm.select(deselect=True)  # root_joint.select()
    head_joints = rgmk.builder.joint_chain(head_locators)
    neck_joint = head_joints[0]

    # attaching the torso and head chain to the root joint
    root_joint.select()
    spine_joints = rgmk.builder.joint_chain(spine_locators)

    # selects the chest_joint and attaches the arms to the chest
    pm.select(deselect=True)  # root_joint.select()
    fl_leg_joints, fl_rv_foot_joints = mk_limb_chain()
    shoulder_joint = fl_leg_joints[0]

    pm.select(deselect=True)
    pelvis_joint = rgmk.builder.joint_chain(pelvis_locators, return_root=True)

    # selects the hip_joint and attaches the legs to the hips
    pelvis_joint.select()
    bl_leg_joints, bl_rv_foot_joints = mk_limb_chain(posterior=True)
    hip_joint = bl_leg_joints[0]

    pm.select(deselect=True)
    tail_joints = rgmk.builder.joint_chain(tail_locators)
    tail_joint = tail_joints[0]

    """
    Orient joints
    """
    rgmk.utils.orient_joint_chain(root_joint)
    rgmk.utils.orient_joint_chain(shoulder_joint)
    rgmk.utils.orient_joint_chain(hip_joint)
    rgmk.utils.orient_joint_chain(neck_joint)
    rgmk.utils.orient_joint_chain(tail_joint)
    rgmk.utils.orient_joint_chain(fl_rv_foot_joints[0])
    rgmk.utils.orient_joint_chain(bl_rv_foot_joints[0])

    """
    Parent to bones group
    """
    pm.parent(root_joint, rig.bones)
    pm.parent(shoulder_joint, rig.bones)
    pm.parent(pelvis_joint, rig.bones)  # parent pelvis instead of hip
    pm.parent(neck_joint, rig.bones)
    pm.parent(tail_joint, rig.bones)
    pm.parent(fl_rv_foot_joints[0], rig.bones)
    pm.parent(bl_rv_foot_joints[0], rig.bones)

    """
    Mirror Joints
    """
    mirror_kwargs = {
        "mirrorYZ": True,
        "mirrorBehavior": True,
        "searchReplace": (L, R)
    }

    pm.mirrorJoint(hip_joint, **mirror_kwargs)
    pm.mirrorJoint(shoulder_joint, **mirror_kwargs)
    pm.mirrorJoint(fl_rv_foot_joints[0], **mirror_kwargs)
    pm.mirrorJoint(bl_rv_foot_joints[0], **mirror_kwargs)

    pm.select(deselect=True)

    """
    Make IKs
    """

    ik, curve, clusters = rgmk.kinematics.spline_ik(root_joint, spine_joints[-1])
    pm.parent(ik, rig.bones)
    pm.parent(curve, rig.extras)
    [pm.parent(cl, rig.extras) for cl in clusters]

    ik, curve, clusters = rgmk.kinematics.spline_ik(neck_joint, head_joints[1])
    pm.parent(ik, rig.bones)
    pm.parent(curve, rig.extras)
    [pm.parent(cl, rig.extras) for cl in clusters]

    ik, curve, clusters = rgmk.kinematics.spline_ik(tail_joint, tail_joints[-1])
    pm.parent(ik, rig.bones)
    pm.parent(curve, rig.extras)
    [pm.parent(cl, rig.extras) for cl in clusters]

    # BACK LEGS
    for side in (L, R):
        rgmk.kinematics.spring_ik(jn(side, B, "ankle", JNT),
                                  jn(side, B, "stifle", JNT),
                                  jn(side, B, "hip", JNT),
                                  parent=rig.extras)

    # FRONT LEGS
    for side in (L, R):
        iknul, iksc = rgmk.kinematics.singleChain_ik(jn(side, "scapula", JNT),
                                                     jn(side, "shoulder", JNT),
                                                     parent=rig.bones)

        #pm.group(iknul, name=jn(iknul.nodeName(), GRP))

        rgmk.kinematics.rotatePlane_ik(jn(side, "wrist", JNT),
                                       jn(side, "shoulder", JNT),
                                       parent=rig.bones)

        rgmk.kinematics.singleChain_ik(jn(side, "wrist", JNT),
                                       jn(side, "ball", JNT),
                                       parent=rig.bones)

        rgmk.kinematics.singleChain_ik(jn(side, "ball", JNT),
                                       jn(side, "toe", JNT),
                                       parent=rig.bones)

    """
    Set up Reverse Foot Chains
    """

    # FRONT LEGS
    prefixes = (L, R)
    rfoot = ("wrist", "ball", "toe")

    for side in prefixes:
        for name in rfoot:
            rgmk.constraints.point_constrain(jn(side, RV, name, JNT),
                                             jn(side, name, IKH, NUL),
                                             null=False)

    # BACK LEGS (using springIK)
    for side in prefixes:
        rgmk.constraints.point_constrain(jn(side, B, RV, "ankle", JNT),
                                         jn(side, B, "ankle", SPNG, NUL),
                                         null=False)

        for name in rfoot[1:]:
            rgmk.constraints.point_constrain(jn(side, B, RV, name, JNT),
                                             jn(side, B, name, JNT),
                                             null=False)

    """
    Constraints and Controls
    """

    tmp_ctrl = rgmk.controls.cube(jn("tail", CTRL), width=0.6)
    rgmk.utils.snap_to(tmp_ctrl, jn("tail05", JNT))

    tail_clusters = find("tail_clusterHandle*", transforms=True)
    print(tail_clusters)
    for target in tail_clusters[1:]:
        target_ctrl = rgmk.controls.cube(target.nodeName().replace(JNT, CTRL), width=0.4)
        nul, __ = rgmk.constraints.parent_constrain(target_ctrl,
                                                    target,
                                                    snap=True)
        rgmk.constraints.parent_constrain(tmp_ctrl, nul, null=False)
        pm.parent(nul, rig.ctrls)
    nul = rgmk.utils.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)

    # Pelvis and hips
    tmp_ctrl = rgmk.controls.copy_template(jn("pelvis", CTRL), "pelvis")
    rgmk.utils.snap_to(tmp_ctrl, pelvis_joint)

    for side in (L, R):
        target_ctrl = rgmk.controls.circle(jn(side, B, "hip", CTRL),
                                              normal=[1, 0, 0],
                                              radius=0.3)
        multiplier = 1 if side == L else -1;
        nul, __ = rgmk.constraints.parent_constrain(target_ctrl,
            jn(side, B, "hip", JNT),
            snap=True,
            offset=[multiplier * 0.3, 0.1, 0])

        rgmk.constraints.parent_constrain(tmp_ctrl, nul, null=False)
        pm.parent(nul, rig.ctrls)

    rgmk.constraints.parent_constrain(tmp_ctrl, pelvis_joint, null=False)
    nul = rgmk.utils.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)

    # Shoulders and scapulas
    tmp_ctrl = rgmk.controls.copy_template(jn("chest", CTRL), "chest")
    rgmk.utils.snap_to(tmp_ctrl, root_joint)

    for side in (L, R):
        target_ctrl = rgmk.controls.circle(jn(side, "scapula", CTRL),
                                              normal=[1, 0, 0],
                                              radius=0.3)
        multiplier = 1 if side == L else -1;
        nul, __ = rgmk.constraints.parent_constrain(target_ctrl,
            jn(side, "scapula", JNT),
            snap=True,
            offset=[multiplier * 0.3, 0.1, 0])

        rgmk.constraints.parent_constrain(tmp_ctrl, nul, null=False)
        pm.parent(nul, rig.ctrls)

        shoulder_ctrl = rgmk.controls.circle(jn(side, "shoulder", CTRL),
                                             normal=[1, 0, 0],
                                             radius=0.25)
        nul, __ = rgmk.constraints.parent_constrain(shoulder_ctrl,
            jn(side, "shoulder", IKH, NUL),
            snap=True,
            offset=[multiplier * 0.3, 0.1, 0])
        rgmk.constraints.parent_constrain(target_ctrl, nul, null=False)
        pm.parent(nul, rig.ctrls)



    nul = rgmk.utils.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)



    # Only front uses Polevector because back uses Spring IK
    for side in (L, R):
        tmp_ctrl = rgmk.controls.copy_template(jn(side, "wrist", PLVC),
                                               "polevector", scale=[1, 1, 1])
        nul, __ = rgmk.constraints.polevector_constrain(
            tmp_ctrl,
            jn(side, "wrist", IKH),
            snap=jn(side, "elbow", JNT),
            offset=[0, 0, -1],
            pivot="snap")

        pm.parent(nul, rig.ctrls)

    # All heel controls
    for side in (L, R, jn(L, B), jn(R, B)):
        tmp_ctrl = rgmk.controls.copy_template(jn(side, RV, "heel", CTRL),
                                               "parent", scale=[1, 1, 1])
        nul, __ = rgmk.constraints.parent_constrain(tmp_ctrl,
                                                    jn(side, RV, "heel", JNT),
                                                    snap=True)

        pm.parent(nul, rig.ctrls)

    # Head control
    tmp_ctrl = rgmk.controls.circle(jn("head01", CTRL),
                                    radius=0.8,
                                    normal=[0, 0, 1])
    rgmk.utils.snap_to(tmp_ctrl, "head01_JNT")

    neck_clusters = find(jn("neck", "_clusterHandle", "*"), transforms=True)
    for target in neck_clusters:
        rgmk.constraints.parent_constrain(tmp_ctrl,
                                          target,
                                          null=False)

    nul = rgmk.utils.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)
    pm.group(nul, name=jn(nul.nodeName(), "_GRP"))

    # Face control
    tmp_ctrl = rgmk.controls.circle(jn("head02", CTRL),
                                    radius=0.8,
                                    normal=[0, 0, 1])
    nul , __ = rgmk.constraints.parent_constrain(tmp_ctrl,
                                                 "head01_JNT",
                                                 snap="head02_JNT",
                                                 pivot="head01_JNT")
    rgmk.constraints.parent_constrain("head01_CTRL", nul)
    pm.parent(nul, rig.ctrls)


    # center of gravity control
    tmp_ctrl = rgmk.controls.circle(jn("spine", CTRL), radius=0.56)

    # The center of gravity control and any of the end controls will probably
    # all need snapping
    # FIXME: hardcoded target
    rgmk.utils.snap_to(tmp_ctrl, "spine02_JNT")
    pm.move(tmp_ctrl, 0, 1.0, 0, relative=True)
    pm.scale(tmp_ctrl, 1, 1, 1.2)
    pm.rotate(tmp_ctrl, -10, 0, 0)

    # Parent all spine children
    # FIXME: Attaching joints directly
    # FIXME: Fix hardcoding
    spine_clusters = find(jn("ROOT", "_clusterHandle", "*"), transforms=True)
    tail_cluster = find(jn("tail", "_clusterHandle", "*"), transforms=True, first_only=True)
    others = [jn("pelvis", CTRL, NUL),
              jn("chest", CTRL, NUL),
              jn("head01", CTRL, NUL, GRP),
              jn("tail", CTRL)]
    spine_ctrl_targets = spine_clusters + [tail_cluster] + others

    for target in spine_ctrl_targets:
        rgmk.constraints.parent_constrain(tmp_ctrl, target, null=False)
    nul = rgmk.utils.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)

    # set default ctrl overrides
    rgmk.controls.set_overrides()

    pm.select(deselect=True)
    return


def mk_limb_chain(posterior=False):

    limb_root, limb_mirror = (("hip*", jn(L, B)) if posterior
                              else ("scapula*", L))

    if posterior:
        limb_chain = rgmk.utils.list_to_chain([limb_root, "stifle*", "hock*",
                                              "ankle*", "ball*"],
                                              mirror=limb_mirror)
    else:
        limb_chain = rgmk.utils.list_to_chain([limb_root, "shoulder*", "elbow*",
                                              "wrist*", "ball*"],
                                              mirror=limb_mirror)

    limb_locators = find(limb_chain, transforms=True)
    limb_joints = rgmk.builder.joint_chain(limb_locators)

    branch_joint = rgmk.utils.get_end_of_chain("ball", limb_joints)

    # NOTE: the find call is further encapsulated in a list to emulate
    # a future method that will split the call into multiple appendages
    # based on unique and enumerated values
    branch = [find(jn(limb_mirror, "toe*", LOC))]

    # making the joints for each finger and attaching to wrist
    for appendage in branch:
        branch_joint.select()
        appendage_joints = rgmk.builder.joint_chain(appendage)
        limb_joints.append(appendage_joints)

    pm.select(deselect=True)
    reverse_foot_names = ["heel", "toe", "ball"]

    if posterior:
        reverse_foot_names.append("ankle")
    else:
        reverse_foot_names.append("wrist")

    reverse_foot_locators = rgmk.utils.list_to_chain(reverse_foot_names,
                                                     mirror=jn(limb_mirror, RV))
    reverse_foot_joints = rgmk.builder.joint_chain(reverse_foot_locators)

    # Deselecting prevents the next joint chain created from being connected
    # to this one
    pm.select(deselect=True)

    return (limb_joints, reverse_foot_joints)
