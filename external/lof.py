import pymel.core as pm

from . import generic as rgmk
from .generic import L, R, B, RV
from .generic import LOC, GRP, NUL, JNT, CTRL, IKH, PLVC, SPNG
from .generic import find, jn


def init_rig(rig_name="Biped", mctrl_name=jn("main", CTRL)):

    rig_node = find(rig_name, first_only=True, transforms=True)
    rig = None

    if not rig_node:
        print("Could not find rig {0}".format(rig_name))
        print("Making one for you")
        rig = rgmk.mk_base_hierarchy(rig_name)

        # Make controls here if necessary

        mctrl = rgmk.mk_control_curve(mctrl_name, radius=3)
        rgmk.attach_main_control(rig, mctrl)
    else:
        print("Rig {0} found as {1}".format(rig_name, rig_node.longName()))
        rig = rgmk.MayaRig(rig_name,
                           find(jn("scale", NUL), first_only=True),
                           find(jn("pos", NUL), first_only=True),
                           find(jn("rot", NUL), first_only=True),
                           find(jn("geo", GRP), first_only=True),
                           find(jn("bones", GRP), first_only=True),
                           find(jn("ctrls", GRP), first_only=True),
                           find(jn("extras", GRP), first_only=True))

    pm.select(rig.name, replace=True)
    return rig


def mk_body(rig):

    root_locator = find(jn("ROOT", LOC), first_only=True, transforms=True)
    root_joint = rgmk.mk_joint_chain([root_locator], return_root=True)

    root_up = rgmk.chainlist(["neck", "head"])
    root_down = rgmk.chainlist(["spine", "pelvis", "tail"])

    head_locators = find(root_up, transforms=True)
    spine_locators = find(root_down, transforms=True)

    # attaching the hips to the root joint
    pm.select(deselect=True)  # root_joint.select()
    head_joints = rgmk.mk_joint_chain(head_locators)
    neck_joint = head_joints[0]

    # attaching the torso and head chain to the root joint
    root_joint.select()
    spine_joints = rgmk.mk_joint_chain(spine_locators)

    # selects the chest_joint and attaches the arms to the chest
    pm.select(deselect=True)  # root_joint.select()
    fl_leg_joints, fl_rv_foot_joints = mk_limb_chain()
    shoulder_joint = fl_leg_joints[0]

    # selects the hip_joint and attaches the legs to the hips
    pm.select(deselect=True)  # hip_joint.select()
    bl_leg_joints, bl_rv_foot_joints = mk_limb_chain(posterior=True)
    thigh_joint = bl_leg_joints[0]

    """
    Orient joints
    """
    rgmk.orient_joint_chain(root_joint)
    rgmk.orient_joint_chain(shoulder_joint)
    rgmk.orient_joint_chain(thigh_joint)
    rgmk.orient_joint_chain(neck_joint)

    """
    Parent to bones group
    """
    pm.parent(root_joint, rig.bones)
    pm.parent(shoulder_joint, rig.bones)
    pm.parent(thigh_joint, rig.bones)
    pm.parent(neck_joint, rig.bones)
    pm.parent(fl_rv_foot_joints[0], rig.bones)
    pm.parent(bl_rv_foot_joints[0], rig.bones)

    """
    Mirror Joints
    """
    mirror_kwargs = {
        "mirrorYZ": True,
        "mirrorBehavior": True,
        "searchReplace": (L, R)
    }

    pm.mirrorJoint(thigh_joint, **mirror_kwargs)
    pm.mirrorJoint(shoulder_joint, **mirror_kwargs)
    pm.mirrorJoint(fl_rv_foot_joints[0], **mirror_kwargs)
    pm.mirrorJoint(bl_rv_foot_joints[0], **mirror_kwargs)

    pm.select(deselect=True)

    """
    Make IKs
    """

    ik, curve, clusters = rgmk.mk_spline_ik(root_joint, spine_joints[-1])
    pm.parent(ik, rig.bones)
    pm.parent(curve, rig.extras)
    [pm.parent(cl, rig.extras) for cl in clusters]

    ik, curve, clusters = rgmk.mk_spline_ik(neck_joint, head_joints[1])
    pm.parent(ik, rig.bones)
    pm.parent(curve, rig.extras)
    [pm.parent(cl, rig.extras) for cl in clusters]

    # BACK LEFT LEG
    rgmk.mk_spring_ik(jn(L, B, "ankle", JNT),
                      jn(L, B, "thigh02", JNT),
                      jn(L, B, "thigh01", JNT),
                      parent=rig.extras)

    """
    rgmk.mk_singlechain_ik(jn(L, B, "hip01", JNT),
                           jn(L, B, "thigh01", JNT),
                           parent=parent)
    """

    # BACK RIGHT LEG
    rgmk.mk_spring_ik(jn(R, B, "ankle", JNT),
                      jn(R, B, "thigh02", JNT),
                      jn(R, B, "thigh01", JNT),
                      parent=rig.extras)

    """
    rgmk.mk_singlechain_ik(jn(R, B, "hip01", JNT),
                           jn(R, B, "thigh02", JNT),
                           parent=parent)
    """

    # FRONT LEFT LEG
    rgmk.mk_rotateplane_ik(jn(L, "ankle", JNT),
                           jn(L, "thigh01", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(L, "shoulder01", JNT),
                           jn(L, "thigh01", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(L, "ankle", JNT),
                           jn(L, "ball", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(L, "ball", JNT),
                           jn(L, "toe", JNT),
                           parent=rig.bones)

    # FRONT RIGHT LEG
    rgmk.mk_rotateplane_ik(jn(R, "ankle", JNT),
                           jn(R, "thigh01", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(R, "shoulder01", JNT),
                           jn(R, "thigh01", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(R, "ankle", JNT),
                           jn(R, "ball", JNT),
                           parent=rig.bones)

    rgmk.mk_singlechain_ik(jn(R, "ball", JNT),
                           jn(R, "toe", JNT),
                           parent=rig.bones)

    """
    Set up Reverse Foot Chains
    """

    # FRONT LEGS
    prefixes = [L, R]
    rfoot = ["ankle", "ball", "toe"]

    for side in prefixes:
        for name in rfoot:
            rgmk.mk_point_constraint(jn(side, RV, name, JNT),
                                     jn(side, name, IKH, NUL),
                                     null=False)

    # BACK LEGS
    for side in prefixes:
        rgmk.mk_point_constraint(jn(side, B, RV, "ankle", JNT),
                                 jn(side, B, "ankle", SPNG, NUL),
                                 null=False)

        for name in rfoot[1:]:
            rgmk.mk_point_constraint(jn(side, B, RV, name, JNT),
                                     jn(side, B, name, JNT),
                                     null=False)

    """
    Constraints
    """

    for side in (L, R):
        tmp_ctrl = rgmk.cp_ctrl_template(jn(side, "ankle", PLVC),
                                         "polevector", scale=[1, 1, 1])
        nul, __ = rgmk.mk_polevector_constraint(
            tmp_ctrl,
            jn(side, "ankle", IKH),
            offset_target=jn(side, "knee", JNT),
            offset=[0, 0, 3])

        pm.parent(nul, rig.ctrls)

    for side in (L, R, jn(L, B), jn(R, B)):
        tmp_ctrl = rgmk.cp_ctrl_template(jn(side, RV, "heel", CTRL),
                                         "parent", scale=[1, 1, 1])
        nul, __ = rgmk.mk_parent_constraint(
            tmp_ctrl,
            jn(side, RV, "heel", JNT),
            snap=True)

        pm.parent(nul, rig.ctrls)

    # center of gravity control
    tmp_ctrl = rgmk.cp_ctrl_template(jn("spine", CTRL),
                                     "gravcenter", scale=[1, 1, 1])
    clusters = find(jn("ROOT", "_clusterHandle", "*"), transforms=True)
    others = [jn(L, B, "hip01", JNT),
              jn(R, B, "hip01", JNT),
              jn(L, "shoulder01", JNT),
              jn(R, "shoulder01", JNT),
              "neck_clusterHandle01"]
    spine_ctrl_targets = clusters + others

    for target in spine_ctrl_targets:
        rgmk.mk_parent_constraint(tmp_ctrl, target, null=False)
    nul = rgmk.null_with_group(tmp_ctrl)
    pm.parent(nul, rig.ctrls)

    # Head and Neck controls
    """
    tmp_ctrl = rgmk.mk_control_curve(jn("head01", CTRL),
                                     radius=0.8,
                                     normal=[0, 0, 1])
    nul, __ = rgmk.mk_parent_constraint(tmp_ctrl,
                                        jn("head01", JNT),
                                        snap=True,
                                        offset=[0, -0.1, 0.4])
    """
    """
    rgmk.mk_parent_constraint(jn("head01", JNT),
                              tmp_ctrl,
                              snap=True,
                              offset=[0, -0.1, 0.4],
                              weight=0.0,
                              null=False)
    pm.parent(nul, rig.ctrls)
    """

    # neck 04
    """
    tmp_ctrl = rgmk.mk_control_curve(jn("neck01", CTRL),
                                     radius=1,
                                     normal=[0, 0.5, 0.5])
    nul, __ = rgmk.mk_parent_constraint(tmp_ctrl,
                                        "neck_clusterHandle03",
                                        offset_target="neck01_JNT")
    pm.parent(nul, rig.ctrls)
    """

    # set default ctrl overrides
    rgmk.set_ctrl_overrides()

    pm.select(deselect=True)
    return

    node = pm.PyNode("LOF")
    cl_01 = pm.PyNode("neck_clusterHandle01")
    cl_02 = pm.PyNode("neck_clusterHandle02")
    cl_03 = pm.PyNode("neck_clusterHandle03")
    cl_04 = pm.PyNode("neck_clusterHandle04")

    try:
        node.addAttr("graze", niceName="Graze", attributeType="double", keyable=True, min=0, max=100)
    except pm.MayaAttributeError:
        pass

    grz = node.graze
    # 01
    pm.setDrivenKeyframe(cl_01.tz, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_01.tz, cd=grz, dv=50.0, value=0.595)
    pm.setDrivenKeyframe(cl_01.tz, cd=grz, dv=100.0, value=1.185)

    pm.setDrivenKeyframe(cl_01.ty, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_01.ty, cd=grz, dv=50.0, value=-0.61)
    pm.setDrivenKeyframe(cl_01.ty, cd=grz, dv=100.0, value=-1.22)

    # 02
    pm.setDrivenKeyframe(cl_02.tz, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_02.tz, cd=grz, dv=50.0, value=1.28)
    pm.setDrivenKeyframe(cl_02.tz, cd=grz, dv=100.0, value=1.78)

    pm.setDrivenKeyframe(cl_02.ty, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_02.ty, cd=grz, dv=50.0, value=0.802)
    pm.setDrivenKeyframe(cl_02.ty, cd=grz, dv=100.0, value=-0.875)

    # 03
    pm.setDrivenKeyframe(cl_03.tz, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_03.tz, cd=grz, dv=50.0, value=2.46)
    pm.setDrivenKeyframe(cl_03.tz, cd=grz, dv=100.0, value=2.5)

    pm.setDrivenKeyframe(cl_03.ty, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_03.ty, cd=grz, dv=50.0, value=-0.014)
    pm.setDrivenKeyframe(cl_03.ty, cd=grz, dv=100.0, value=-2.1)

    # 04
    pm.setDrivenKeyframe(cl_04.tz, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_04.tz, cd=grz, dv=50.0, value=2.88)
    pm.setDrivenKeyframe(cl_04.tz, cd=grz, dv=100.0, value=3.345)

    pm.setDrivenKeyframe(cl_04.ty, cd=grz, dv=0.0, value=0.0)
    # pm.setDrivenKeyframe(cl_04.ty, cd=grz, dv=50.0, value=-1)
    pm.setDrivenKeyframe(cl_04.ty, cd=grz, dv=100.0, value=-3.1)

    pm.select(deselect=True)


def mk_limb_chain(posterior=False):

    limb_root, limb_mirror = (("hip*", jn(L, B)) if posterior
                              else ("shoulder*", L))

    limb_chain = rgmk.chainlist([limb_root, "thigh*", "knee*",
                                "ankle*", "ball*"],
                                mirror=limb_mirror)

    limb_locators = find(limb_chain, transforms=True)
    limb_joints = rgmk.mk_joint_chain(limb_locators)

    branch_joint = rgmk.get_end_of_chain("ball", limb_joints)

    # NOTE: the find call is further encapsulated in a list to emulate
    # a future method that will split the call into multiple appendages
    # based on unique and enumerated values
    branch = [find(jn(limb_mirror, "toe*", LOC))]

    # making the joints for each finger and attaching to wrist
    for appendage in branch:
        branch_joint.select()
        appendage_joints = rgmk.mk_joint_chain(appendage)
        limb_joints.append(appendage_joints)

    pm.select(deselect=True)
    reverse_foot_names = ["heel", "toe", "ball", "ankle"]
    reverse_foot_locators = rgmk.chainlist(reverse_foot_names,
                                           mirror=jn(limb_mirror, RV))
    reverse_foot_joints = rgmk.mk_joint_chain(reverse_foot_locators)

    # Deselecting prevents the next joint chain created from being connected
    # to this one
    pm.select(deselect=True)

    return (limb_joints, reverse_foot_joints)
