# Amy Richards
# March 2012

import maya.cmds as cmds

# mirror prefixes
L = "L_"
R = "R_"

# naming convention suffixes
LOC = "_LOC"    # Locators
GEO = "_GEO"    # Geo
GRP = "_GRP"    # Group
NUL = "_NUL"    # Null
JNT = "_JNT"    # Joint
CTRL = "_CTRL"  # Control
IK = "_IK"      # IK
PV = "_PV"      # Pole Vector
CP = "_CP"      # Copy


def mk_joints(locator_list):
    """
    for each item in the list of locators, it finds the position of that item,
    creates a joint at that position, and name the joint after the item with
    the suffix "_JNT"
    """
    # make an empty list to hold the joint chain
    chain = []

    for item in locator_list:
        position = cmds.getAttr(item + '.translate')[0]

        # After we've found the locator, replace LOC with JNT at the end
        # of the string
        if item.endswith(LOC):
            item = item.replace(LOC, JNT)

        joint = cmds.joint(p=position, name=item)

        # show local axes for checking joint orientation
        #cmds.toggle(joint, localAxis=True)

        # adding the new joint into the list
        chain.append(joint)

    # return the list of new joints
    return chain


def orient_child_joints(parent_joint):
    """
    Applies the same oreintation recursively to all joints except the last
    in th chain
    """

    # lists all the child joints of the current parent joint
    children = cmds.listRelatives(parent_joint,
                                  children=True,
                                  type="joint")

    # if the list is not empty, the current parent joint is actually a parent
    if children:

        # then orient it like a parent joint
        cmds.joint(parent_joint, edit=True, zso=True, oj='xyz', sao='yup', ch=True)

        # and for each of its children
        for child_joint in children:

            # run this function again with each child as the new parent
            orient_child_joints(child_joint)

    # if the list is empty, the "parent_joint" was not actually a parent,
    # but an end joint
    else:
        # alias the parent joint so we don't get confused
        end_joint = parent_joint

        # Set the last joint in the chain to (0,0,0) so
        # it will follow its parent
        #cmds.joint(parent_joint, edit=True, zso=True, oj='xyz', sao='yup')
        cmds.joint(end_joint, edit=True, o=(0, 0, 0))
        #print("end of chain", end_joint)


def mk_body(parent=None):

    # Since there is only one root locator, cmds.ls is making sure
    # it exists
    root = cmds.ls("root"+LOC, transforms=True)
    # making root joint and saving a reference to it
    root_joint = mk_joints(root)[0]

    # listing locators of all the body parts that do not need to be mirrored
    # hips is by itself so we can easily access the last hip joint to attach
    # the legs
    hip_locators = cmds.ls(["hip*"+LOC], transforms=True)

    # torso is by itself so we can easily access the last chest joint for
    # attaching the arms
    torso_locators = cmds.ls(["back*"+LOC, "chest*"+LOC], transforms=True)

    # Neck and head are the last locators left in the chain
    head_locators = cmds.ls(["neck*"+LOC, "head*"+LOC], transforms=True)

    # attaching the hips to the root joint
    cmds.select(root_joint, replace=True)
    hip_joints = mk_joints(hip_locators)

    # attaching the torso and head chain to the root joint
    cmds.select(root_joint, replace=True)
    torso_joints = mk_joints(torso_locators)
    head_joints = mk_joints(head_locators)

    # Index -1 is the last item in the list
    # The last item in torso_joints is the chest
    chest_joint = torso_joints[-1]

    # The first item in hip_joints is the hip joint the legs will be attached
    # to
    hip_joint = hip_joints[0]

    # selects the chest_joint and attaches the arms to the chest
    cmds.select(chest_joint, replace=True)
    shoulder_joint = mk_arm()

    # selects the hip_joint and attaches the legs to the hips
    cmds.select(hip_joint, replace=True)
    thigh_joint = mk_leg()

    ## At this point, the left half of the rig is built.

    # starts at the root joint and makes its way down the rig
    # at each joint, if the joint is a child, it uses its parent's orientation
    orient_child_joints(root_joint)

    """
    Mirroring joints now
    """

    # Mirrors the mk_leg joints (whose root is the hip)
    # mirrorBehavior properly flips the orientations
    # mirrorYZ mirrors across the x axis
    cmds.mirrorJoint(thigh_joint,
                     mirrorYZ=True,
                     mirrorBehavior=True,
                     searchReplace=(L, R))

    # Mirrors the mk_arm joints (whose root is the chest)
    cmds.mirrorJoint(shoulder_joint,
                     mirrorYZ=True,
                     mirrorBehavior=True,
                     searchReplace=(L, R))

    # Deselecting prevents the next joint chain created from being connected
    # to this one
    cmds.select(deselect=True)

    if parent is not None:
        # this parents the root joint to the object passed into mk_body,
        # if any
        cmds.parent(root_joint, parent)

    """
    Mesh specific values unti the end of this function
    """

    ## Make IK Handles
    mk_ik()
    cmds.select(deselect=True)

    ## Making pole vector constraints
    L_wrist_polevec = mk_translate_ctrl(L+"shoulder"+PV, scale=[1, 1, 4])
    mk_polevec_constraint(L+"wrist"+IK,
                          L+"elbow"+JNT,
                          L_wrist_polevec,
                          offset=[0, -4, -3])

    R_wrist_polevec = mk_translate_ctrl(R+"wrist"+PV, scale=[1, 1, 4])
    mk_polevec_constraint(R+"wrist"+IK,
                          R+"elbow"+JNT,
                          R_wrist_polevec,
                          offset=[0, -4, -3])


    L_ankle_polevec = mk_translate_ctrl(L+"ankle"+PV, scale=[1, 1, 3])
    mk_polevec_constraint(L+"ankle"+IK,
                          L+"knee"+JNT,
                          L_ankle_polevec,
                          offset=[0, 0, 2])

    R_ankle_polevec = mk_translate_ctrl(R+"ankle"+PV, scale=[1, 1, 3])
    mk_polevec_constraint(R+"ankle"+IK,
                          R+"knee"+JNT,
                          R_ankle_polevec,
                          offset=[0, 0, 2])

    cmds.select(deselect=True)

    ## Rotation and translation controllers
    #######################################
    root_ctrl = mk_translate_ctrl("root"+CTRL, scale=[20, 2, 14])
    mk_point_constraint(root_ctrl, root_joint)

    hip_ctrl = mk_orient_ctrl("hip"+CTRL,
                              root_joint,
                              radius=4,
                              normal=[0, 1, 0])[0]
    mk_orient_constraint(hip_ctrl, root_joint)

    # Neck 1
    name = mk_orient_ctrl("neck_1"+CTRL,
                          "neck_1"+JNT,
                          radius=3,
                          normal=[0, 1, 1])[0]
    mk_orient_constraint(name, "neck_1"+JNT)

    # Neck 2
    name = mk_orient_ctrl("neck_2"+CTRL,
                          "neck_2"+JNT,
                          radius=3,
                          normal=[0, 1, 1])[0]
    mk_orient_constraint(name, "neck_2"+JNT)

    # Head 1
    name = mk_orient_ctrl("head_1"+CTRL,
                          "head_2"+JNT,
                          radius=3,
                          normal=[0, 0, 1])[0]
    mk_orient_constraint(name, "head_1"+JNT)
    # Snap head 1 control to head 2 joint
    snap_point_constraint("head_2"+JNT, "head_1"+CTRL)
    move_and_freeze("head_1"+CTRL, position=[0, -1, 0])

    # Back
    back_ctrl = mk_orient_ctrl("back"+CTRL,
                               "back"+JNT,
                               radius=4,
                               normal=[0, 1, 0])[0]
    mk_orient_constraint(back_ctrl, "back"+JNT)

    # Chest
    chest_ctrl = mk_orient_ctrl("chest"+CTRL,
                                "chest"+JNT,
                                radius=5.5,
                                normal=[0, 1, 0])[0]
    mk_orient_constraint(chest_ctrl, "chest"+JNT)

    # Left Shoulder
    #pivot = cmds.xform(L+"shoulder"+JNT,
    #                   query=True,
    #                   worldSpace=True,
    #                   translation=True)
    #gname = cmds.group([L+"shoulder"+JNT], name=L+"shoulder"+GRP)
    #L_shoulder_ctrl = mk_orient_ctrl(L+"shoulder"+CTRL,
    #                          L+"shoulder"+GRP,
    #                         radius=2,
    #                          normal=[1,0,0])[0]
    #cmds.xform(gname, pivots=pivot)
    #mk_orient_constraint(L_shoulder_ctrl, L+"shoulder"+GRP)


    # Right Shoulder
    #R_shoulder_ctrl = mk_orient_ctrl(R+"shoulder"+CTRL,
    #                          R+"shoulder"+JNT,
    #                          radius=2,
    #                          normal=[1,0,0])[0]
    #mk_orient_constraint(R_shoulder_ctrl, R+"shoulder"+JNT)

    # Head 2 (DON'T USE)
    #pivot = cmds.xform("head_1"+JNT,
    #                   query=True,
    #                   worldSpace=True,
    #                   translation=True)
    #cmds.xform(name, pivots=pivot)

    # Left Wrist
    name = mk_translate_ctrl(L+"wrist"+CTRL, scale=[2.5, 2.5, 2.5])
    mk_orient_constraint(name, L+"wrist"+JNT)
    mk_point_constraint(name, L+"wrist"+IK+GRP)

    # Both Orientation and Point constraints map to the same
    # wrist control, but order seems to matter. It seems the best
    # way to avoid cyclic dependencies and undefined movement is
    # to do the orienation always first, and to put the orient constraint
    # on the JNT directly, with the point constraint on the parent group

    # Right Wrist
    name = mk_translate_ctrl(R+"wrist"+CTRL, scale=[2.5, 2.5, 2.5])

    mk_orient_constraint(name, R+"wrist"+JNT)
    mk_point_constraint(name, R+"wrist"+IK+GRP)

     # Left Ankle
    name = mk_translate_ctrl(L+"ankle"+CTRL, scale=[4, 4, 4])
    mk_orient_constraint(name, L+"ankle"+JNT)
    mk_point_constraint(name, L+"ankle"+IK+GRP)

    # Right Ankle
    name = mk_translate_ctrl(R+"ankle"+CTRL, scale=[4, 4, 4])
    mk_orient_constraint(name, R+"ankle"+JNT)
    mk_point_constraint(name, R+"ankle"+IK+GRP)

    cmds.select(deselect=True)


def mk_leg():

    # listing locators of body parts that make up a leg
    leg_locators = cmds.ls([L+"thigh*"+LOC, L+"knee*"+LOC, L+"ankle*"+LOC,
                            L+"ball*"+LOC], transforms=True)

    # make joints for each body part in the leg and save
    # those joints in a list
    leg_joints = mk_joints(leg_locators)

    # Index -1 is the last item in the list
    # The last item in leg_joints is the ball
    ball_joint = leg_joints[-1]
    # the first item in leg_joints list is thigh
    thigh_joint = leg_joints[0]

    # make lists of locators for each of the toes
    rightToe_locators = cmds.ls([L + "rightToe*"+LOC], transforms=True)
    middleToe_locators = cmds.ls([L + "middleToe*"+LOC], transforms=True)
    leftToe_locators = cmds.ls([L + "leftToe*"+LOC], transforms=True)

    # put each toe list into a foot list that we can loop over
    foot = [rightToe_locators, middleToe_locators, leftToe_locators]

    # making the joints for each toe and attaching to ball
    for toe_locators in foot:
        cmds.select(ball_joint, replace=True)
        mk_joints(toe_locators)

    # Deselecting prevents the next joint chain created from being connected
    # to this one
    cmds.select(deselect=True)

    return leg_joints[0]


def mk_arm():

    # listing locators of body parts that make up an arm
    arm_locators = cmds.ls([L+"shoulder*"+LOC, L+"elbow*"+LOC, L+"wrist*"+LOC],
                           transforms=True)

    # make joints for each body part in the arm
    arm_joints = mk_joints(arm_locators)

    # Index -1 is the last item in the list
    # The last item in arm_joints is the wrist
    wrist_joint = arm_joints[-1]
    # the first item in arm_joints is shoulder
    shoulder_joint = arm_joints[0]

    # make lists of locators for each of the fingers
    thumb_locators = cmds.ls([L + "thumb_*"+LOC], transforms=True)
    index_locators = cmds.ls([L + "index_*"+LOC], transforms=True)
    middle_locators = cmds.ls([L + "middle_*"+LOC], transforms=True)
    pinky_locators = cmds.ls([L + "pinky_*"+LOC], transforms=True)

    # put each finger list into a hand list that we can loop over
    hand = [thumb_locators, index_locators, middle_locators, pinky_locators]

    # making the joints for each finger and attaching to wrist
    for finger in hand:
        cmds.select(wrist_joint, replace=True)
        mk_joints(finger)

    # Deselecting prevents the next joint chain created from being connected
    # to this one
    cmds.select(deselect=True)

    return arm_joints[0]


def mk_ik():
    """
    Helping function to group all the IK control handles
    """
    # ik handles that will use pole vectors have to use the RP solver
    # default_solver = "ikSCsolver"

    ik_group = []

    ikl = mk_ik_handle(L+"thigh"+JNT, L+"ankle"+JNT)
    ikr = mk_ik_handle(R+"thigh"+JNT, R+"ankle"+JNT)
    ik_group.append(ikl)
    ik_group.append(ikr)

    ikl = mk_ik_handle(L+"shoulder"+JNT, L+"wrist"+JNT)
    ikr = mk_ik_handle(R+"shoulder"+JNT, R+"wrist"+JNT)
    ik_group.append(ikl)
    ik_group.append(ikr)

    # This would need to be altered if there are additional neck joints added
    ikl = mk_ik_handle("neck_1"+JNT, "neck_2"+JNT)
    ik_group.append(ikl)


def mk_ik_handle(start_joint, end_joint, solver="ikRPsolver"):
    """
    Makes a single IK Handle with the pivots of its associated joints
    on a parent group
    """
    pivot = cmds.xform(end_joint,
                       query=True,
                       worldSpace=True,
                       translation=True)

    name = end_joint.replace(JNT, IK)
    name, effector = cmds.ikHandle(n=name, sj=start_joint, ee=end_joint,
                                   sol=solver)
    group_name = cmds.group(n=name+GRP)
    cmds.xform(group_name, pivots=pivot)

    cmds.parent(group_name, "ctrls"+GRP)


def mk_bind():
    """
    Looks for all geometry named as GEO and attempts to parent it to
    a joint of the same name, should one exist
    """

    cmds.select(deselect=True)

    for item in cmds.ls("*"+GEO):
        # In this case, replace() is just changing the string value
        # of the geo's name to look for a joint of the same name
        joint = item.replace(GEO, JNT)
        joint = joint.split("|")[-1]

        if cmds.ls(joint):
            item_copy = cmds.duplicate(item, name=item+CP)

            item_copy = cmds.parent(item_copy, "geo"+GRP)

            cmds.parentConstraint(joint, item_copy, mo=True)

    for item in cmds.ls("*|werewolf_GRP|werewolf_*"):
        # In this case, replace() is just changing the string value
        # of the geo's name to look for a joint of the same name

        item_copy = cmds.duplicate(item, name=item+CP)
        item_copy = cmds.parent(item_copy, "geo"+GRP)

        cmds.parentConstraint("head_1"+JNT, item_copy, mo=True)


def mk_translate_ctrl(name, scale=None):
    """
    Makes a new translate-only control (probably a box)
    """
    if scale is None:
        scale = [1, 1, 1]

    translate_CTRL_TPL = cmds.ls("translate"+CTRL)[0]

    copied_ctrl = cmds.duplicate(translate_CTRL_TPL, name=name)[0]
    cmds.parent(copied_ctrl, world=True)
    sx, sy, sz = scale

    cmds.setAttr(copied_ctrl+".scale", sx, sy, sz, type="double3")
    cmds.makeIdentity(copied_ctrl, apply=True, s=True)

    cmds.parent(copied_ctrl, "ctrls"+GRP)

    return copied_ctrl


def mk_orient_ctrl(name, target_joint, radius=1, normal=[1, 0, 0]):
    """
    Makes a new orientation and translation control circle
    """

    pivots = cmds.xform(target_joint, query=True, ws=True, t=True)

    circle_ctrl = cmds.circle(name=name,
                              center=pivots,
                              radius=radius,
                              normal=normal,
                              constructionHistory=False)
    cmds.xform(cp=True)
    #cmds.xform(piv=pivots)
    cmds.makeIdentity(circle_ctrl, apply=True, r=True, t=True, s=True)

    cmds.parent(circle_ctrl, "ctrls"+GRP)

    return circle_ctrl


def mk_polevec_constraint(ik_name, joint_name, ctrl_name, offset=None):
    """
    Makes an poleVectorConstraint for the given ik handle. The pole vector
    will move to the given joint's location with an optional offset and
    use the given control name
    """

    if offset is None:
        offset = [0, 0, 0]

    # Temporarily constrain the wrong way to get the control
    # to the joint
    snap_point_constraint(joint_name, ctrl_name)

    ox, oy, oz = offset
    cmds.move(ox, oy, oz, ctrl_name, relative=True)

    cmds.makeIdentity(ctrl_name, apply=True, r=True, t=True, s=True)
    cmds.poleVectorConstraint(ctrl_name, ik_name,
                              name='_'.join([ctrl_name,
                              'poleVectorConstraint']))


def snap_point_constraint(object1, object2):
    # Temporarily constrain to snap to another joint
    cmds.delete(cmds.pointConstraint(object1, object2))


def mk_point_constraint(ctrl_name, target_joint):
    """
    Makes an pointConstraint for the given control at the given joint location
    """
    snap_point_constraint(target_joint, ctrl_name)

    return cmds.pointConstraint(ctrl_name,
                                target_joint,
                                name='_'.join([ctrl_name, 'pointConstraint']),
                                maintainOffset=True)


def mk_orient_constraint(ctrl_name, target_joint):
    """
    Makes an orientConstraint for a control at the given joint location
    """
    snap_point_constraint(target_joint, ctrl_name)
    return cmds.orientConstraint(ctrl_name,
                                 target_joint,
                                 name='_'.join([ctrl_name,
                                                'orientConstraint']),
                                 maintainOffset=True)


def move_and_freeze(obj, position=[0, 0, 0]):
    px, py, pz = position
    cmds.move(px, py, pz, obj, relative=True)
    cmds.makeIdentity(obj, apply=True, r=True, t=True, s=True)


def create_rig(name="Biped"):
    """
    The main function which creates the hierarchy and calls all other
    helping functions
    """
    # The name of the Rig and its top level group in Maya
    rig_name = name

    # looking for the existence of rig_name
    if cmds.ls(rig_name):
        # if found, delete rig_name and all of its children
        cmds.delete(rig_name, hierarchy="below")

    # create top level group
    rig_name = cmds.group(name=rig_name, empty=True)

    scale_NUL = cmds.group(name="scale"+NUL,
                           empty=True,
                           parent=rig_name)

    pos_NUL = cmds.group(name="pos"+NUL,
                         empty=True,
                         parent=scale_NUL)

    rot_NUL = cmds.group(name="rot"+NUL,
                         empty=True,
                         parent=pos_NUL)

    geo_GRP = cmds.group(name="geo"+GRP,
                         empty=True,
                         parent=rot_NUL)

    bones_GRP = cmds.group(name="bones"+GRP,
                           empty=True,
                           parent=rot_NUL)

    ctrls_GRP = cmds.group(name="ctrls"+GRP,
                           empty=True,
                           parent=rot_NUL)

    main_CTRL = cmds.circle(name="main"+CTRL,
                            normal=(0, 1, 0),
                            center=(0, 0, 0),
                            radius=9)[0]  # add [0] because it returns a list

    cmds.parent(main_CTRL, rig_name)

    # Make constraints for the top NULs
    # '_'.join connects the items of a list with an underscore in between
    cmds.scaleConstraint(main_CTRL, scale_NUL,
                         name='_'.join([scale_NUL, 'scaleConstraint']))
    cmds.pointConstraint(main_CTRL, pos_NUL,
                         name='_'.join([pos_NUL, 'pointConstraint']))
    cmds.orientConstraint(main_CTRL, rot_NUL,
                          name='_'.join([rot_NUL, 'orientConstraint']))

    cmds.select(deselect=True)  # make sure nothing is selected

    # Delete existing joints first
    existing_joints = cmds.ls(type="joint")
    if existing_joints:
        cmds.delete(existing_joints, hierarchy="below")

    mk_body(parent=bones_GRP)

    mk_bind()
